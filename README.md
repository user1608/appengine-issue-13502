# appengine-issue-13502
A minimal working example for google appengine issue 13502

For details please refer to https://code.google.com/p/googleappengine/issues/detail?id=13502

Sample links:

https://www.majorbuild.com/issue-13502-basic - without http2 server push, works normally
https://www.majorbuild.com/issue-13502-push - with http2 server push, as of 2017-01-18, most of the time it fails with net::ERR_SPDY_PROTOCOL_ERROR in Chrome console log, sometimes it behaves like there is no server push, rarely it works without any fault with server push. Earlier around 2017-01-05 server push was working without any problem.
https://majorbuild2017.appspot.com/issue-13502-basic - same sample as the "basic" version above, only without custom domain, works normally
https://majorbuild2017.appspot.com/issue-13502-push - sample sample as the "push" version above, works like "basic", no server push

